# -*- coding:utf-8 -*-

from CustomDesigns import CustomDesign
import CustomConfig as cf
from werkzeug.utils import secure_filename
cd = CustomDesign(verbose=True)
import os
# 列举规则 测试
#
# print("列举规则")
# list = cd.listRule(cf.custom_designColl, userID="ltt", ownerID= "ltt", botID="ltt")
#
# # 添加规则 测试。
#
# print("从文件中添加规则")
# cd.setRuleFromFile(cf.custom_designColl, "owner", "bot", "alice", "rule_file")
# print("添加单条规则")
# cd.setSingleRule(cf.custom_designColl, "owner", "alice", "bob", "^.*你好呀$", "你好吗")
# print("批量添加规则")
# ruleList = [["^.*你好    $", "你好"], ["^.*你好吗$","你好吗"], ["^.*你好\?$#","你好?"], ["^.*你好china\?$", " 你好?"]]
# cd.setRuleBatch(cf.custom_designColl, "owner", "alice", "bob", ruleList)
# print("\n")
# customlist = cd.setRuleBatch(collection=cf.custom_designColl, ownerID="zhm", userID="zhm",
#                              botID="zhm", ruleList=[["^.*你好 $", "你好"], ["^.*你好吗$","你好吗"], ["^.*你好\?$#","你好?"], ["^.*你好china\?$", " 你好?"]])
# print("Custom Design :\n")


if __name__ == "__main__":
   result, rev = cd.match(ownerid="ltt",userid="ltt",botid="ltt",timemark="1234",moduletype="Custom Design", query="你好")
   print (result)
   print (rev)