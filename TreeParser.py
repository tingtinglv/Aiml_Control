# -*- coding:utf-8 -*-
import os
import json

import Utils_string as string
import TreeConfig
from Node import Node
from MongoDB import _mongoDB
from TreeDB import _TreeDB
from Utils_errors import *

class TreeParser(object):
	def __init__(self, ptable=None, stable=None, atable=None, ctable=None):
		self._p_table = ptable or TreeConfig.tree_collection
		self._s_table = stable or TreeConfig.sentence_collection
		self._a_table = atable or TreeConfig.selection_collection
		self._c_table = ctable or TreeConfig.condition_collection
		self.trees = []

	def _readTreeFromDB(self, node_id):
		"""
		输入某个节点的id
		返回以该节点为根节点的树
		:param node_id: 根节点 id
		:return:  a instance of Node
		"""
		node = _mongoDB.search(self._p_table, {TreeConfig.node_id: node_id}, {"_id": 0}, [("_id", 1)], 1, 0)[0]
		sid = node[TreeConfig.sentence_id]
		node_type = node[TreeConfig.node_type]
		node_operation = node[TreeConfig.node_operation]
		node_is_leaf = node[TreeConfig.node_leaf]
		node_is_root = node[TreeConfig.node_root]
		sentence = _mongoDB.search(
			self._s_table,
			{TreeConfig.sentence_id: sid},
			{"_id": 0, "Content": 1},
			[("_id", 1)], 1, 0
		)[0]["Content"]
		tree = Node(node_id, sentence, node_type, node_is_leaf, node_is_root, node_operation)

		# 两种节点: condition 与 selection
		if node_type == TreeConfig.condition_collection:
			# condition
			for condition_id, next_node in node[TreeConfig.node_AN].items():
				condition_id = int(condition_id)
				condition = _mongoDB.search(
					self._c_table,
					{TreeConfig.condition_id: condition_id},
					{"_id": 0},
					[("_id", 1)], 1, 0
				)[0]["Content"]
				tree.addCondition(condition, self._readTreeFromDB(next_node))
		elif node_type == TreeConfig.selection_type:
			# selection
			for selection_id, next_node in node[TreeConfig.node_AN].items():
				selection_id = int(selection_id)
				selection = _mongoDB.search(
					self._a_table,
					{TreeConfig.selection_id: selection_id},
					{"_id": 0},
					[("_id", 1)], 1, 0
				)[0]["Content"]
				tree.addChild(selection, self._readTreeFromDB(next_node))
		return tree

	def builtTree(self, filters={TreeConfig.node_root: True}):
		"""
		暂时弃用

		依据条件 filters 从数据库中获取匹配的对话树 并存于self._trees

		:return:
		"""
		tree_list = _mongoDB.search(self._p_table, filters, {"_id": 0}, [("_id", 1)], 1, 0)
		for ele in tree_list:
			tree = self._readTreeFromDB(ele[TreeConfig.node_id])
			self.trees.append(tree)
		return self.trees

	def readTreeFromDB(self, tree_id):
		"""
		输入某个节点的id
		返回以该节点为根节点的树
		:param tree_id: 根节点 id
		:return:  a instance of Node
		"""
		try:
			tree = self._readTreeFromDB(tree_id)
			return tree
		except PyMongoTimeOutError as e:
			print("Tree Parser:\n\tMongoDB Error: {0}".format(e))
			return None

	def _readTreeFromJSON(self, dict_tree):
		try:
			nid = dict_tree["id"]
			if TreeConfig.selection_type in dict_tree:
				tree_type = TreeConfig.selection_type
				sentence = dict_tree["value"]
				if len(dict_tree[TreeConfig.selection_type]) == 0:
					return Node(nid, sentence, tree_type, True)
				tree = Node(nid, sentence, tree_type)
				for subtree in dict_tree[tree_type]:
					answer = dict_tree[tree_type][subtree]["value"]
					tree.addChild(answer, self._readTreeFromJSON(dict_tree[tree_type][subtree]["node"]))
				return tree
			elif TreeConfig.condition_type in dict_tree:
				tree_type = TreeConfig.condition_type
				sentence = ""
				tree = Node(nid, sentence, tree_type)
				for subtree in dict_tree[tree_type]:
					condition = dict_tree[tree_type][subtree]["value"]
					tree.addCondition(condition, self._readTreeFromJSON(dict_tree[tree_type][subtree]["node"]))
				return tree
			else:
				sentence = dict_tree["value"]
				tree_type = TreeConfig.selection_type
				tree = Node(nid, sentence, tree_type, True)
				return tree
		except:
			return None

	def readTreeFromJSON(self, json_str):
		"""
		输入一个json 形式的对话树，生成一个对话树
		:param json_str: json 形式的对话树
		:return: a instance of Node
		"""
		try:
			if isinstance(json_str, str):
				dict_tree = json.loads(json_str)
			elif isinstance(json_str, unicode):
				dict_tree = json.loads(json_str.encode("utf-8"))
			elif isinstance(json_str, dict):
				dict_tree = json_str
			else:
				return None
			tree = self._readTreeFromJSON(dict_tree)
			tree._is_root = True
			self.trees.append(tree)
			return tree
		except:
			return None

	def _saveAsJSON(self, tree):
		node = {}
		node["id"] = tree._node_id
		if tree._type == TreeConfig.selection_type:
			_type = TreeConfig.selection_type
			node["value"] = tree._sentence
			node[_type] = {}
			for idx, answer in enumerate(tree._children):
				_tmp_key = "answer_" + str(idx)
				node[_type][_tmp_key] = {}
				node[_type][_tmp_key]["value"] = answer
				node[_type][_tmp_key]["node"] = self._saveAsJSON(tree._children[answer]) if tree._children[answer] else None
		elif tree._type == TreeConfig.condition_type:
			_type = TreeConfig.condition_type
			node[_type] = {}
			for idx, condition in enumerate(tree._conditions):
				_tmp_key = "condition_" + str(idx)
				node[_type][_tmp_key] = {}
				node[_type][_tmp_key]["value"] = condition
				node[_type][_tmp_key]["node"] = self._saveAsJSON(tree._children[idx]) if tree._children[idx] else None
		return node

	def saveAsJSON(self, tree):
		"""
		输入一个 对话树 返回 该树的 json形式
		:param tree:  a instance of Node
		:return: json of tree
		"""
		try:
			retV = json.dumps(self._saveAsJSON(tree))
			return retV
		except PyMongoTimeOutError as e:
			print("Tree Parser:\n\tMongoDB Error: {0}".format(e))
			return None
	def _saveIntoDB(self, tree, is_update):
		if not tree:
			return None
		node_sentence_id = _TreeDB.setContent(self._s_table, TreeConfig.sentence_id, tree._sentence)
		node_type = tree._type
		node_leaf = tree._is_leaf
		node_root = tree._is_root
		node_operation = tree._operation or ""
		# subnode update
		node_AN = {}
		if node_type == TreeConfig.condition_type:
			for idx, ele in enumerate(tree._conditions):
				condition_id = _TreeDB.setContent(self._c_table, TreeConfig.condition_id, ele)
				subnode_id = self._saveIntoDB(tree._children[idx], is_update)
				node_AN[str(condition_id)] = int(subnode_id)
		elif node_type == TreeConfig.selection_type:
			for ele in tree._children:
				selection_id = _TreeDB.setContent(self._a_table, TreeConfig.selection_id, ele)
				subnode_id = self._saveIntoDB(tree._children[ele], is_update)
				node_AN[str(selection_id)] = int(subnode_id)
		# 当 is_update 为 True 时 node_id 有效，反之无效
		node_id = tree._node_id
		result = _TreeDB.setNode(
			self._p_table,
			node_sentence_id,
			node_AN,
			node_root,
			node_leaf,
			node_type,
			node_operation,
			is_update,
			node_id
		)
		return int(result)

	def saveIntoDB(self, tree, is_update=False):
		"""
		输入一个对话树 将其存入数据库
		:param tree: a instance of Node
		:param is_update: flag mean update or insert
		:return: 返回存入数据库时树的id 用于再次获取
		"""
		try:
			result_id = self._saveIntoDB(tree, is_update)
			return result_id
		except PyMongoTimeOutError as e:
			print("Tree Parser:\n\tMongoDB Error: {0}".format(e))


	def _saveIntoFile(self, tree, tree_inf={}):
		tree_ls = []
		tree_ls.append(str(int(tree._node_id)))
		tree_ls.append(tree._type)
		tree_inf["sentences"].append(tree._sentence)
		tree_ls.append(str(int(len(tree_inf["sentences"]) - 1)))
		key = []
		value = []
		for idx, ele in enumerate(tree._children):
			sub_node_str, tree_inf = self._saveIntoFile(tree._children[ele], tree_inf)
			if tree._type == TreeConfig.selection_type:
				tree_inf["selections"].append(ele)
				key.append(str(int(len(tree_inf["selections"])-1)))
				value.append(sub_node_str)
			elif tree._type == TreeConfig.condition_type:
				content = string.list2str(tree._conditions[idx], "conditions")
				# tree_inf["conditions"].append(tree._conditions[idx])
				tree_inf["conditions"].append(content)
				key.append(str(int(len(tree_inf["conditions"])-1)))
				value.append(sub_node_str)
		ky_ls = "[" + ",".join(key) + "]"
		tree_ls.append(ky_ls)
		vl_ls = "[" + ",".join(value) + "]"
		tree_ls.append(vl_ls)
		retV = "(" + "||".join(tree_ls) + ")"
		return retV, tree_inf

	def saveIntoFile(
			self, tree,
			t_file_name="trees.txt",
			s_file_name="sentences.txt",
			a_file_name="selections.txt",
			c_file_name="conditions.txt"
	):
		try:
			file_list = {
				t_file_name: "tree_str",
				s_file_name: "sentences",
				a_file_name: "selections",
				c_file_name: "conditions"
			}
			tree_inf = {
				"selections": [],
				"conditions": [],
				"sentences": []
			}
			tree_str, tree_inf = self._saveIntoFile(tree, tree_inf)

			# 树结构表
			with open(t_file_name, "w") as f:
				f.write(tree_str)

			file_list.pop(t_file_name)
			# 文本表
			for file_name in file_list:
				with open(file_name, "w") as f:
					content_type = file_list[file_name]
					for idx, content in enumerate(tree_inf[content_type]):
						line = str(idx) + "#@#@#@" + content + "\n"
						line = line.encode("utf-8") if isinstance(line, unicode) else line
						f.write(line)

		except Exception as e:
			print(e)
			return None

	def _readFromFile(self, tree_str="", tree_inf={}):
		tree_str = tree_str[1:-1]
		tree_str = tree_str.split("||", 4)
		node_id = int(string.ignoreWS(tree_str[0]))
		node_type = string.ignoreWS(tree_str[1])
		sentence_id = int(string.ignoreWS(tree_str[2]))
		node_sentence = tree_inf["sentences"][sentence_id]
		sub_node_key = string.str2list(tree_str[3], "int")
		sub_node_str = string.str2list(tree_str[4], "tree")
		tree = Node(node_id, node_sentence, node_type)
		if node_type == TreeConfig.condition_type:
			for idx, condition_id in enumerate(sub_node_key):
				content = tree_inf["conditions"][condition_id]
				content = string.str2list(content, "conditions")
				sub_tree = self._readFromFile(sub_node_str[idx], tree_inf)
				tree.addCondition(content, sub_tree)
		elif node_type == TreeConfig.selection_type:
			for idx, selection_id in enumerate(sub_node_key):
				content = tree_inf["selections"][selection_id]
				sub_tree = self._readFromFile(sub_node_str[idx], tree_inf)
				tree.addChild(content, sub_tree)
		if len(sub_node_key) == 0:
			tree._is_leaf = True
		return tree

	def readFromFile(
			self,
			t_file_name="trees.txt",
			s_file_name="sentences.txt",
			a_file_name="selections.txt",
			c_file_name="conditions.txt"
	):
		"""
		从文件中获取建树内容
		:param t_file_name: 存树结构的文件
		:param s_file_name: 存机器语句的文件
		:param a_file_name: 存选项的文件
		:param c_file_name: 存条件的文件
		:return: 返回一个 Node 的实例
		"""
		assert (os.path.isfile(t_file_name))
		assert (os.path.isfile(s_file_name))
		assert (os.path.isfile(a_file_name))
		assert (os.path.isfile(c_file_name))
		try:
			tree_inf = {
				"sentences": {},
				"selections": {},
				"conditions": {}
			}

			# 树结构表
			with open(t_file_name) as f:
				tree_str = f.readline()
			tree_str = tree_str.decode("utf-8") if isinstance(tree_str, str) else tree_str

			file_list = {
				s_file_name: {},
				a_file_name: {},
				c_file_name: {}
			}
			# 文本表
			for ele in file_list:
				with open(ele, 'r') as f:
					for line in f:
						line = line.split("#@#@#@")
						idx = int(string.ignoreWS(line[0]))
						content = string.ignoreWS(line[1])
						content = content.decode("utf-8") if isinstance(content, str) else content
						file_list[ele][idx] = content

			tree_inf["sentences"] = file_list[s_file_name]
			tree_inf["selections"] = file_list[a_file_name]
			tree_inf["conditions"] = file_list[c_file_name]

			tree = self._readFromFile(tree_str, tree_inf)
			tree._is_root = True
			return tree
		except IOError as e:
			print ("Tree Parser:\n\tIO Error: {0}".format(e))

if __name__ == "__main__":
	"""
	测试树在文件中的效果。
	存在问题：
		因为浅拷贝的原因，
		在Node的实例 tree 被函数调用存入文件后会导致:
			实例 tree 的 conditions 内容由 [[],..,[]] 变成 ['[]','[]']
			在该情况下将 tree 存入数据库后获取将无法正确解析 conditions 内容
		考虑每次存储 tree 之前先将整个 tree 进行深拷贝会有性能影响, 暂时未作处理
		目前建议存储 tree 后不再对其做其他操作。
	"""
	tp = TreeParser()
	###############
	# 建树        #
	###############

	# 1.文件中获取树

	tree = tp.readFromFile(
		t_file_name="trees.txt",
		s_file_name="sentences.txt",
		a_file_name="selections.txt",
		c_file_name="conditions.txt"
	)

	# 2.根据json解析树

	# with open("tree_json.txt", 'r') as f:
	# 	json_str = f.readline()
	# tree = tp.readTreeFromJSON(json_str)

	# 3.从数据库中获取树

	# tree = tp.readTreeFromDB(26)

	###############
	# 存树        #
	###############

	# 1. 将树存入文件
	# tp.saveIntoFile(
	# 	tree=tree,
	# 	t_file_name="trees2.txt",
	# 	s_file_name="sentences2.txt",
	# 	a_file_name="selections2.txt",
	# 	c_file_name="conditions2.txt"
	# )

	# 2.将树存成json格式
	# json_str = tp.saveAsJSON(tree)
	# with open("tree_json2.txt", 'w') as f:
	# 	f.write(json_str)

	# 3.将树存入数据库
	tree_id = tp.saveIntoDB(tree)
	print tree_id

	# 树的运行测试在 DialogTree.py