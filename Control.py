# -*- coding: utf-8 -*-
#!/usr/bin/env python
# @Time    : 2017/12/4 10:22
# @Author  : tignting.lv
# @Site    : 
# @File    : Control.py
# @Software: PyCharm
#@Contact  : sunfiyes@163.com
import Config as cf
from SortConfig import sortOrder
def strNone(conext):
    if conext.strip() == "" or conext == None:
        conext = cf.DEFAULT_ID
    return conext
def isNone(userId, ownerId, botId):
    userId = strNone(userId)
    ownerId = strNone(ownerId)
    botId = strNone(botId)
    return userId, ownerId, botId
def control(userId, ownerId, botId, context):
    if context.strip() == "" or context == None:
        return "输入内容不能为空"
    debug, userId, ownerId, botId, history, response = sortOrder(userId, ownerId, botId, context)
    return debug, userId, ownerId, botId, history, response