#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2017/11/12 15:19
# @Author  : tignting.lv
# @Site    : 
# @File    : QuestionConfig.py
# @Software: PyCharm
#@Contact  : sunfiyes@163.com
from Config import *
question_table = "question"
dialog_table = "dialog"
moduleType = "Custom Design"
AIMLTYPE = "AIML"
CUSTOMTYPE = "CUSTOMTYPE"
TREETYPE = "TREETYPE"
DEFAULT_TITLE = "DEFAULT_TITLE"
DEFAULT_CONTEXT = "DEFAULT_CONTEXT"
DEFAULT_ID = "DEFAULT_ID"