# -*- coding:utf-8 -*-
import TreeConfig
import Utils_time as time
from MongoDB import _mongoDB


class TreeDB(object):
	def __init__(self, verbose=False):
		self._verbose = verbose
		self._historyColl = TreeConfig.historyColl
		self._featureColl = TreeConfig.featureColl
		self._filter_list = {
			self._historyColl: {
				"ownerID": TreeConfig.DEFAULT_ID,
				"userID": TreeConfig.DEFAULT_ID,
				"botID": TreeConfig.DEFAULT_ID
			},
			self._featureColl: {
				"ownerID": TreeConfig.DEFAULT_ID,
				"userID": TreeConfig.DEFAULT_ID,
				"botID": TreeConfig.DEFAULT_ID
			}
		}
		self._proj_list = {
			self._historyColl: {"_id": 0, "treeID": 1},
			self._featureColl: {"_id": 0, "value": 1}
		}
		self._sort_by = {
			self._historyColl: [("datetime", -1)],
			self._featureColl: [("datetime", -1)]
		}

	def getHistory(self, collection, ownerID, userID, botID):
		try:
			filters = {}
			if ownerID:
				filters["ownerID"] = ownerID
			if userID:
				filters["userID"] = userID
			if botID:
				filters["botID"] = botID
			cursor = _mongoDB.search(
				collection,
				filters,
				self._proj_list[self._historyColl],
				self._sort_by[self._historyColl],
				1
			)
			return cursor[0]["treeID"]
		except IndexError as e:
			print ("TreeDB:\n\t getHistory: {0}".format(e))
			return None

	def saveHistory(self, collection, ownerID, userID, botID, treeID):
		datetime = time.getCurrentTime()
		data = {
			"ownerID": ownerID,
			"userID": userID,
			"botID": botID,
			"treeID": treeID,
			"datetime": datetime
		}
		result = _mongoDB.insert(collection, data)
		if self._verbose:
			print result

	def getFeature(self, collection, name, ownerID, userID, botID):
		try:
			self._filter_list[self._featureColl]["ownerID"] = ownerID
			self._filter_list[self._featureColl]["userID"] = userID
			self._filter_list[self._featureColl]["botID"] = botID
			self._filter_list[self._featureColl]["name"] = name
			cursor = _mongoDB.search(
				collection,
				self._filter_list[self._featureColl],
				self._proj_list[self._featureColl],
				self._sort_by[self._featureColl],
				1
			)
			return cursor[0]["value"]
		except IndexError as e:
			print("TreeDB:\n\t getFeature: {0}".format(e))
			return ""

	def setFeature(self, collection, name, value, ownerID, userID, botID):
		datetime = time.getCurrentTime()
		data ={
			"userID": userID,
			"botID": botID,
			"ownerID": ownerID,
			"name": name,
			"value": value,
			"datetime": datetime
		}
		result = _mongoDB.insert(collection, data)
		return result


	def setNode(self, collection, SentencID, AN, Root, Leaf, Type, Operation, is_update=False, NID=None):
		try:
			if not is_update:
				# 获取一个新的nodeID
				cursor = _mongoDB.search(
					collection,
					{},
					{TreeConfig.node_id: 1},
					[(TreeConfig.node_id, -1)],
					1
				)
				if cursor:
					NID = cursor[0][TreeConfig.node_id] + 1
				else:
					NID = 0
			data = {
				TreeConfig.node_id: NID,
				TreeConfig.sentence_id: SentencID,
				TreeConfig.node_AN: AN,
				TreeConfig.node_root: Root,
				TreeConfig.node_leaf: Leaf,
				TreeConfig.node_type: Type,
				TreeConfig.node_operation: Operation
			}
			if is_update:
				result = _mongoDB.update({TreeConfig.node_id: NID}, data)
			else:
				result = _mongoDB.insert(collection, data)
			if self._verbose:
				print(result)
			if result:
				return int(NID)
			else:
				return None
		except KeyError as e:
			print ("TreeDB:\n\tMongoDB 中可能含有错误数据 :{0}".format(e))
			raise
		except IndexError as e:
			print("TreeDB:\n\tIndex Error in setting Node: {0}".format(e))
			raise

	def setContent(self, collection, id_type, content, is_update=False, ID=None):
		try:
			# 判断数据库是否已经存在,是则直接返回对应的id
			cursor = _mongoDB.search(
				collection,
				{"Content": content},
				{id_type: 1},
				[(id_type, -1)],
				1
			)
			if cursor and cursor.count() != 0:
				return int(cursor[0][id_type])

			if not is_update:
				# 获取一个新的记录 ID
				cursor = _mongoDB.search(
					collection,
					{},
					{id_type: 1},
					[(id_type, -1)],
					1
				)
				if cursor and cursor.count() != 0:
					ID = cursor[0][id_type] + 1
				else:
					return None
			data = {
				id_type: ID,
				"Content": content
			}
			if is_update:
				result = _mongoDB.update(collection, {id_type: ID}, data)
			else:
				result = _mongoDB.insert(collection, data)
			if self._verbose:
				print(result)
			return int(ID)
		except KeyError as e:
			print("TreeDB:\n\tMongoDB中可能含有异常数据".format(e))
			raise
		except IndexError as e:
			print("TreeDB:\n\t IndexError in setting content :{0}".format(e))
			raise
		except Exception as e:
			print (e)
			raise

_TreeDB = TreeDB()