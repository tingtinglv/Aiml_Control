# -*- coding:utf-8 -*-
from Config import *
from TreeConditions import *

# 默认模块类型表示
moduletype = "Dialog Tree"
# 用户历史记录表
historyColl = "dialogTreeHistory"
# 用户信息表
featureColl = "dialogTreeFeature"

# 对话树建立所需的4张表名字
tree_collection = "tree"
sentence_collection = "sentence"
selection_collection = "selection"
condition_collection = "condition"


# 各表中 各项名字
node_id = "NID"
node_AN = "A&N"
node_type = "Type"
node_operation = "Operation"
sentence_id = "SentenceID"
condition_id = "ConditionID"
selection_id = "SelectionID"
node_leaf = "Leaf"
node_root = "Root"


# node 类型选项
selection_type = "selection"
condition_type = "condition"

default_type = selection_type

# 对话树 条件函数名在数据库的存储表示

DEFAULT_PARA = u"TRUE"
DEFAULT_USER_PARA = u"TRUE"
DEFAULT_TRUE = u"TRUE"
IS_EMPTY = u"IS_EMPTY"
IS_HAVE = u"IS_HAVE"
IS_EQUAL = u"IS_EQUAL"
IS_GT = u"IS_GT"
IS_LT = u"IS_LT"
IS_GE = u"IS_GE"
IS_LE = u"IS_LE"

# 对话树条件函数映射

parser_condition = {
	DEFAULT_TRUE: default_true,
	IS_EMPTY: is_empty,
	IS_HAVE: not is_empty,
	IS_EQUAL: is_equal,
	IS_GT: is_gt,
	IS_LT: is_lt,
	IS_GE: not is_lt,
	IS_LE: not is_gt
}